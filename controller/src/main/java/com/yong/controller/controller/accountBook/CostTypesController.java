package com.yong.controller.controller.accountBook;

import com.yong.common.response.BaseResponse;
import com.yong.dao.domain.CostType;
import com.yong.dao.domain.CostTypes;
import com.yong.service.accountBook.CostTypeService;
import com.yong.service.accountBook.CostTypesService;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;


@CrossOrigin
@RestController
@RequestMapping("accountBook")
public class CostTypesController {
    @Resource
    private CostTypeService costTypeService;

    @RequestMapping("getCostTypes")
    public BaseResponse getCostTypes(@RequestParam(defaultValue = "1") Integer current,
                                     @RequestParam(defaultValue = "10") Integer size,
                                     @RequestParam(defaultValue = "tree") String mode) {
        return costTypeService.getCostType(current, size, mode);
    }

    @RequestMapping("addCostTypes")
    public BaseResponse addCostTypes(CostType costType) {
        return costTypeService.addCostType(costType);
    }

    @RequestMapping("alterCostTypes")
    public BaseResponse alterCostTypes(CostType costType) {
        return costTypeService.alterCostType(costType);
    }

    @RequestMapping("deleteCostTypes")
    public BaseResponse deleteCostTypes(CostType costType) {
        return costTypeService.deleteCostType(costType);
    }
}
