package com.yong.service.accountBook;

import com.yong.common.response.BaseResponse;
import com.yong.dao.domain.CostType;

public interface CostTypeService {
    public BaseResponse getCostType(Integer current, Integer size, String mode);

    public BaseResponse addCostType(CostType costType);

    public BaseResponse alterCostType(CostType costType);

    public BaseResponse deleteCostType(CostType costType);
}
