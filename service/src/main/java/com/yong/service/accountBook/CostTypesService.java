package com.yong.service.accountBook;

import com.yong.common.response.BaseResponse;
import com.yong.dao.domain.CostTypes;

import java.util.List;

public interface CostTypesService {
    public BaseResponse getCostType(Integer current, Integer size);

    public BaseResponse addCostTypes(CostTypes costTypes);

    public BaseResponse alterCostTypes(CostTypes costTypes);

    public BaseResponse deleteCostTypes(CostTypes costTypes);
}
