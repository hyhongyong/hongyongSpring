package com.yong.service.accountBook;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.ArrayUtils;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.yong.common.constants.Constants;
import com.yong.common.response.BaseResponse;
import com.yong.dao.domain.CostType;
import com.yong.dao.domain.CostTypeCascade;
import com.yong.dao.domain.CostTypeTree;
import com.yong.dao.mapper.CostTypeMapper;
import org.junit.Assert;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

@Service
public class CostTypeServiceImpl implements CostTypeService {
    private static Integer id = 1;

    @Resource
    private CostTypeMapper costTypeMapper;

    public BaseResponse getCostType(Integer current, Integer size, String mode) {
        BaseResponse baseResponse = new BaseResponse();
        try {
            QueryWrapper<CostType> wrapper = new QueryWrapper<>();
            wrapper.eq("parent_id", 0).orderByAsc("type");
            List<CostType> costTypeList = costTypeMapper.selectAll(wrapper);
            Assert.assertNotNull(Constants.SELECT_FAIL, costTypeList);
            Assert.assertNotEquals(Constants.SELECT_NULL, 0, costTypeList.size());
            baseResponse.setCode(Constants.SUCCESS_CODE);
            id = 1;
            if ("tree".equals(mode)) {
                baseResponse.setData(getCostTypeTree(costTypeList));
            } else if ("cascade".equals(mode)) {
                baseResponse.setData(getCostTypeCascade(costTypeList));
            }
        } catch (Throwable e) {
            baseResponse.setCode(Constants.FAIL_CODE);
            baseResponse.setMsg(e.getClass().getName() + e.getMessage());
        }
        return baseResponse;
    }

    private static List<CostTypeTree> getCostTypeTree(List<CostType> costTypeList) {
        ArrayList<CostTypeTree> costTypeTreeList = new ArrayList<>();
        if (CollectionUtils.isEmpty(costTypeList)) {
            return costTypeTreeList;
        }
        Iterator<CostType> iterator = costTypeList.iterator();
        while (iterator.hasNext()) {
            CostType next = iterator.next();
            CostTypeTree costTypeTree = new CostTypeTree(id++, next.getType(), getCostTypeTree(next.getChildren()));
            costTypeTreeList.add(costTypeTree);
        }
        return costTypeTreeList;
    }

    private static List<CostTypeCascade> getCostTypeCascade(List<CostType> costTypeList) {
        ArrayList<CostTypeCascade> costTypeCascadeList = new ArrayList<>();
        if (CollectionUtils.isEmpty(costTypeList)) {
            return costTypeCascadeList;
        }
        Iterator<CostType> iterator = costTypeList.iterator();
        while (iterator.hasNext()) {
            CostType next = iterator.next();
            CostTypeCascade costTypeCascade = new CostTypeCascade(next.getType(), next.getType(), new ArrayList<>());
            Iterator<CostType> it = next.getChildren().iterator();
            while (it.hasNext()) {
                CostType n = it.next();
                costTypeCascade.getChildren().add(new CostTypeCascade(n.getType(), n.getType(), getCostTypeCascade(n.getChildren())));
            }
            costTypeCascadeList.add(costTypeCascade);
        }
        return costTypeCascadeList;
    }

    public BaseResponse addCostType(CostType costType) {
        BaseResponse baseResponse = new BaseResponse();
        try {
            QueryWrapper<CostType> wrapper = new QueryWrapper<>();
            wrapper.eq("type", costType.getType());
            Assert.assertEquals(Constants.INSERT_DUPLICATE, 0, costTypeMapper.selectList(wrapper).size());
            int insert = costTypeMapper.insert(costType);
            Assert.assertNotEquals(Constants.INSERT_FAIL, 0, insert);
            baseResponse.setCode(Constants.SUCCESS_CODE);
            baseResponse.setData(costType);
        } catch (Throwable e) {
            baseResponse.setCode(Constants.FAIL_CODE);
            baseResponse.setMsg(e.getClass().getName() + e.getMessage());
        }
        return baseResponse;
    }

    public BaseResponse alterCostType(CostType costType) {
        BaseResponse baseResponse = new BaseResponse();
        try {
            int update = costTypeMapper.updateById(costType);
            Assert.assertNotEquals(Constants.UPDATE_FAIL, 0, update);
            baseResponse.setCode(Constants.SUCCESS_CODE);
            baseResponse.setMsg(Constants.UPDATE_SUCCESS);
            baseResponse.setData(costType);
        } catch (Throwable e) {
            baseResponse.setCode(Constants.FAIL_CODE);
            baseResponse.setMsg(e.getClass().getName() + e.getMessage());
        }
        return baseResponse;
    }

    public BaseResponse deleteCostType(CostType costType) {
        BaseResponse baseResponse = new BaseResponse();
        try {
            int delete = costTypeMapper.deleteById(costType.getId());
            Assert.assertNotEquals(Constants.DELETE_FAIL, 0, delete);
            baseResponse.setCode(Constants.SUCCESS_CODE);
            baseResponse.setMsg(Constants.DELETE_SUCCESS);
            baseResponse.setData(costType);
        } catch (Throwable e) {
            baseResponse.setCode(Constants.FAIL_CODE);
            baseResponse.setMsg(e.getClass().getName() + e.getMessage());
        }
        return baseResponse;
    }
}
