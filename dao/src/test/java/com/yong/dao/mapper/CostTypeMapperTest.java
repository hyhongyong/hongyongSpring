package com.yong.dao.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yong.dao.domain.CostType;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.List;

public class CostTypeMapperTest {
    private static CostTypeMapper mapper;

    @BeforeClass
    public static void setUpMybatisDatabase() {
        SqlSessionFactory builder = new SqlSessionFactoryBuilder().build(CostTypeMapperTest.class.getClassLoader().getResourceAsStream("mybatisTestConfiguration/CostTypeMapperTestConfiguration.xml"));
        //you can use builder.openSession(false) to not commit to database
        mapper = builder.getConfiguration().getMapper(CostTypeMapper.class, builder.openSession(true));
    }

    @Test
    public void testSelectAll() {
        QueryWrapper<CostType> wrapper = new QueryWrapper<>();
        wrapper.eq("parent_id", 0);
        List<CostType> costTypeList = mapper.selectAll(wrapper);
        costTypeList.forEach(System.out::println);
    }
}
