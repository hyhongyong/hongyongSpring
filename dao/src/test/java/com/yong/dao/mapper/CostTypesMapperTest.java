package com.yong.dao.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.yong.dao.domain.CostTypes;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.List;

public class CostTypesMapperTest {
    private static CostTypesMapper mapper;

    @BeforeClass
    public static void setUpMybatisDatabase() {
        SqlSessionFactory builder = new SqlSessionFactoryBuilder().build(CostTypesMapperTest.class.getClassLoader().getResourceAsStream("mybatisTestConfiguration/CostTypesMapperTestConfiguration.xml"));
        //you can use builder.openSession(false) to not commit to database
        mapper = builder.getConfiguration().getMapper(CostTypesMapper.class, builder.openSession(true));
    }

    @Test
    public void testSelectAll() {
        QueryWrapper<CostTypes> wrapper = new QueryWrapper<>();
        wrapper.eq("class1", "餐饮类").isNotNull("class2");
        List<CostTypes> costTypes = mapper.selectAll(wrapper);
        costTypes.forEach(System.out::println);
    }
}
