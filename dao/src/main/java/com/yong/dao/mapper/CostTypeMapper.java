package com.yong.dao.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.yong.dao.domain.CostType;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface CostTypeMapper extends BaseMapper<CostType> {
    List<CostType> selectAll(@Param(Constants.WRAPPER) QueryWrapper<CostType> wrapper);
}