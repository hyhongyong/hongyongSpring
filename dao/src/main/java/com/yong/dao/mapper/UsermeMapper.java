package com.yong.dao.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yong.dao.domain.Userme;

public interface UsermeMapper extends BaseMapper<Userme> {
}