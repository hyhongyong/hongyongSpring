package com.yong.dao.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.yong.dao.domain.CostTypes;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface CostTypesMapper extends BaseMapper<CostTypes> {
    List<CostTypes> selectAll(@Param(Constants.WRAPPER) Wrapper<CostTypes> wrapper);
}
