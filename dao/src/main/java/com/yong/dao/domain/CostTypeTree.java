package com.yong.dao.domain;

import java.util.List;

public class CostTypeTree {
    private Integer id;

    private String label;

    private List<CostTypeTree> children;

    public CostTypeTree(Integer id, String label, List<CostTypeTree> children) {
        this.id = id;
        this.label = label;
        this.children = children;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public List<CostTypeTree> getChildren() {
        return children;
    }

    public void setChildren(List<CostTypeTree> children) {
        this.children = children;
    }

    @Override
    public String toString() {
        return "CostTypeTree{" +
                "label='" + label + '\'' +
                ", children=" + children +
                '}';
    }
}