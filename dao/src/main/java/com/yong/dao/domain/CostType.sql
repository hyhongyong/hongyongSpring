-- auto Generated on 2022-02-27
-- DROP TABLE IF EXISTS cost_type;
CREATE TABLE cost_type(
	id INT NOT NULL,
	type VARCHAR (50) NOT NULL,
	parent_id INT NOT NULL,
	PRIMARY KEY (id)
);
