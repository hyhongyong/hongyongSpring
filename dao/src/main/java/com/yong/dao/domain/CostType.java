package com.yong.dao.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.util.List;

@TableName(value = "cost_type")
public class CostType {
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @TableField(value = "\"type\"")
    private String type;

    private List<CostType> children;

    public static final String COL_ID = "id";

    public static final String COL_TYPE = "type";

    public static final String COL_PARENT_ID = "parent_id";

    /**
     * @return id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type
     */
    public void setType(String type) {
        this.type = type;
    }

    public List<CostType> getChildren() {
        return children;
    }

    public void setChildren(List<CostType> children) {
        this.children = children;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", type=").append(type);
        sb.append(", children=").append(children);
        sb.append("]");
        return sb.toString();
    }
}