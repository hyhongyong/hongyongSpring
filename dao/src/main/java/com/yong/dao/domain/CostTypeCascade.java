package com.yong.dao.domain;

import java.util.List;

public class CostTypeCascade {
    private String value;

    private String label;

    private List<CostTypeCascade> children;

    public CostTypeCascade(String value, String label, List<CostTypeCascade> children) {
        this.value = value;
        this.label = label;
        this.children = children;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public List<CostTypeCascade> getChildren() {
        return children;
    }

    public void setChildren(List<CostTypeCascade> children) {
        this.children = children;
    }

    @Override
    public String toString() {
        return "CostTypeCascade{" +
                "value='" + value + '\'' +
                ", label='" + label + '\'' +
                ", children=" + children +
                '}';
    }
}