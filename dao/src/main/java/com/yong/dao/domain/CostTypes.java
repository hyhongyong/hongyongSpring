package com.yong.dao.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

@TableName("cost_types")
public class CostTypes {
    @TableId(type = IdType.AUTO)
    private Integer id;
    private String class1;
    private String class2;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getClass1() {
        return class1;
    }

    public void setClass1(String class1) {
        this.class1 = class1;
    }

    public String getClass2() {
        return class2;
    }

    public void setClass2(String class2) {
        this.class2 = class2;
    }

    @Override
    public String toString() {
        return "CostTypes{" +
                "id=" + id +
                ", class1='" + class1 + '\'' +
                ", class2='" + class2 + '\'' +
                '}';
    }
}
