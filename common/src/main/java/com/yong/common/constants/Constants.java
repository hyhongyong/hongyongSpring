package com.yong.common.constants;

public class Constants {
    public static final String SUCCESS_CODE = "success";
    public static final String SUCCESS_MSG = "成功";
    public static final String FAIL_CODE = "fail";
    public static final String FAIL_MSG = "失败";

    public static final String INSERT_SUCCESS = "新增成功";
    public static final String INSERT_DUPLICATE = "新增失败，已存在相似数据";
    public static final String INSERT_FAIL = "新增失败";
    public static final String DELETE_SUCCESS = "删除成功";
    public static final String DELETE_FAIL = "删除失败";
    public static final String UPDATE_SUCCESS = "修改成功";
    public static final String UPDATE_FAIL = "修改失败";
    public static final String SELECT_SUCCESS = "查询成功";
    public static final String SELECT_NULL = "查询结果为空";
    public static final String SELECT_FAIL = "查询失败";
}
