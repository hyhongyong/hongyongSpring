package com.hongyong.hongyongspring;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@MapperScan("com.yong.dao.mapper")//扫描mapper文件夹
@SpringBootApplication(scanBasePackages = {"com.yong"})
public class HongyongSpringApplication {

    public static void main(String[] args) {
        SpringApplication.run(HongyongSpringApplication.class, args);
    }

}
