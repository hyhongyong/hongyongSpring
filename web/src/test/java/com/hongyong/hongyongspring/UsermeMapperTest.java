package com.hongyong.hongyongspring;

import com.yong.dao.domain.Userme;
import com.yong.dao.mapper.UsermeMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UsermeMapperTest {
    @Resource
    private UsermeMapper usermeMapper;

    @Test
    public void deleteById() {
        int rows = usermeMapper.deleteById(1);
        System.out.println("rows = " + rows);

        List<Userme> usermeList = usermeMapper.selectList(null);
        usermeList.forEach(System.out::println);

        Userme userme = new Userme();
        userme.setId(2);
        userme.setAge(26);
        rows = usermeMapper.updateById(userme);
        System.out.println("userme = " + userme);
    }

    @Test
    public void testOLI() {
        int version = 1;
        Userme userme = new Userme();
        userme.setEmail("hy@qq.com");
        userme.setId(3);
        userme.setVersion(version);
        boolean update = userme.updateById();
        System.out.println("update = " + update);
    }
}